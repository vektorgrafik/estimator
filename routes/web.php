<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::Auth();

Route::get('auth/logout', function() {
	Auth::logout();
	return redirect('/');
});

// Dashboard
Route::get('/', function() { return view('estimate.home'); })->middleware('auth');

// Estimates page
Route::get('/estimates', function () {
    
    $est = new App\Estimate();
    $est->whereNull('groups')->delete();

    return view('estimate.index');

})->middleware('auth');

// Normal visit to a specific estimate
Route::get('/{id}', function( $estimateId ) {

	$estimate = App\Estimate::findOrFail($estimateId);

	return view('estimate.edit', [
		'id' => $estimate->id
	]);

})->middleware('estimateauth');