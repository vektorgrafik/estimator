<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*
|--------------------------------------------------------------------------
| Categories
|--------------------------------------------------------------------------
*/
Route::get('categories', [
    'middleware' => 'auth',
    'uses' => 'ApiController@getAllCategories'
]);

/*
|--------------------------------------------------------------------------
| Clients
|--------------------------------------------------------------------------
*/
Route::get('clients', [
    'middleware' => 'auth',
    'uses' => 'ClientController@all'
]);

/*
|--------------------------------------------------------------------------
| Estimates
|--------------------------------------------------------------------------
*/

// Get single Estimate
Route::get('estimates/{id}', [
    'middleware' => 'estimateauth',
    'uses' => 'ApiController@getEstimateById'
]);

// Get all Estimates
Route::get('estimates', [
    'middleware' => 'auth',
    'uses' => 'ApiController@getAllEstimates'
]);

// Create new Estimate
Route::post('estimates', [
    'middleware' => 'auth',
    'uses' => 'ApiController@createEstimate'
]);

// Update existing Estimate
Route::patch('estimates/{estimate}', [
    'middleware' => 'auth',
    'uses' => 'ApiController@storeEstimate'
]);