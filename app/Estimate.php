<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estimate extends Model
{
    public 		$incrementing = false;
    protected 	$with = ['categories', 'user', 'client'];
    protected 	$hidden = ['pivot'];

    protected 	$casts = [
    	'groups' => 'array'
    ];
    protected   $appends = [
        'created', 'updated'
    ];
    
    protected $fillable = [
    	'client_name', 'description', 'groups', 'tags', 'public', 'approved'
    ];

    public function getCreatedAttribute($value) {
        
        return $this->created_at->diffForHumans();

    }

    public function getUpdatedAttribute($value) {
        
        return $this->updated_at->diffForHumans();

    }

    public function categories() {
    	return $this->belongsToMany('App\Category');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function client() {
        return $this->belongsTo('App\Client');
    }

}