<?php

namespace App\Http\Controllers;

use App\Estimate;
use App\Category;
use App\Client;
use App\Http\Requests;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    
	public function getEstimateById( $id ) {

		return Estimate::findOrFail($id);
		
	}

	public function getAllCategories() {

		return Category::all();
		
	}

	public function getAllEstimates() {

		return Estimate::orderBy('created_at', 'desc')->get();

	}

	public function createEstimate() {

		$client = Client::find(request()->get('client_id'));

		if( !$client ) {
			$client = new Client();
			$client->name = request()->get('client_name');
			$client->save();
		}

		$estimate = new Estimate();
		$estimate->id = uniqid();
		$estimate->user_id = auth()->id();
		$estimate->client()->associate($client);
		$estimate->save();

		return $estimate;

	}

	public function storeEstimate(Estimate $estimate) {

		$estimate->fill(
		    request()->all()
		);

		$estimate->categories()->sync(
		    collect(
		        request()->get('categories')
		    )->pluck('id')->all()
		);

		$client = Client::findOrFail(request()->get('client_id'));
		
		$estimate->client()->associate($client);
		$estimate->save();

		return $estimate;

	}

}
