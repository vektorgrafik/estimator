<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Estimate;

class EstimateAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ( auth()->guest() && ! Estimate::where('id', request()->route('id'))->firstOrFail()->public ) {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}
