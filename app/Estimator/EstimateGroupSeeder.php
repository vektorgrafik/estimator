<?php
namespace App\Estimator;
use Faker\Factory;

class EstimateGroupSeeder {

	public static function seed() {

	    $faker = Factory::create();
	    $groups = [];
	    $estimate = [];
	    $numGroups = mt_rand(1, 8);

	    for($i=0; $i<$numGroups; $i++) {

	        $numRows = mt_rand(2, 12);
	        
	        $group = new \StdClass();
	        $group->title = $faker->word;
	        $group->rows = [];

	        for($y=0; $y<$numRows; $y++) {
	            $row = [
	                'title' => $faker->sentence,
	                'hours' => mt_rand(1, 60),
	                'description' => ''
	            ];
	            array_push($group->rows, $row);
	        }

	        array_push($estimate, $group);

	    }

	    return $estimate;

	}

}