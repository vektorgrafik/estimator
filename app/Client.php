<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name'];
    
	public function estimate() {
		return $this->hasMany('App\Estimate');
	}

}
