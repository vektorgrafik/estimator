<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Estimate::class, function (Faker\Generator $faker) {

    return [
        'id' => $faker->Uuid,
        'client_id' => mt_rand(1,15),
        'description' => $faker->realText(150),
        'groups' => App\Estimator\EstimateGroupSeeder::seed(),
        'tags' => $faker->words(mt_rand(1,5), true),
        'public' => mt_rand(0,1),
        'approved' => mt_rand(0,1),
        'created_at' => $faker->date,
        'updated_at' => $faker->date,
        'user_id' => 1
    ];

});

$factory->define(App\Category::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->word
    ];

});

$factory->define(App\Client::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->company
    ];

});