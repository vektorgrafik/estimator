<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = factory(App\Category::class, 5)->create();
        $clients    = factory(App\Client::class, 15)->create();

        factory(App\Estimate::class, 50)->create()->each(function($estimate) use ($categories, $clients) {
        	
        	$categoryIds = [];
        	for($x=0; $x<mt_rand(1,3); $x++ ) {
	        	$categoryIds[] = $categories->get(mt_rand(0,4))->id;
	        	$estimate->categories()->sync(array_unique($categoryIds));
        	}

        });
    }
}
