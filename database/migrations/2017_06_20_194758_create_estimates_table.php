<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimates', function (Blueprint $table) {
            $table->string('id', 64)->unique();
            $table->integer('user_id');
            $table->integer('client_id');
            $table->text('description')->nullable();
            $table->text('groups')->nullable();
            $table->text('tags')->nullable();
            $table->smallInteger('public')->nullable()->default(0);
            $table->smallInteger('approved')->nullable()->default(0);
            $table->primary('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estimates');
    }
}
