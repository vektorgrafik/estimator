let axios = require('axios');

export function doRequest( method, endpoint, data, callback ) {

	axios({
		method: method,
		url: `/api/${endpoint}`,
		data: data
	})
	.then(function (response) {
		callback(response.data)
	})
  	.catch(function (error) {
    	console.log(error)
  	})
}

export function post( endpoint, data, callback ) {
	doRequest('post', endpoint, data, callback)
}

export function patch( endpoint, data, callback ) {
	doRequest('patch', endpoint, data, callback)
}

export function get( endpoint, callback ) {
	doRequest('get', endpoint, null, callback)
}