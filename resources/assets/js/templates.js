export const templates = [
		
		{
			title: 'Tom grupp',
			template: [
				{
					title: '',
					rows: [
						{ title: '', hours: 0, description: '' }
					]
				}
			]
		},

		{
			title: 'Uppdatering / Vidareutveckling',
			template: [
				{
					title: 'Design',
					rows: [
						{ title: '', hours: 0, description: '' }
					]
				},
				{
					title: 'Frontend-utveckling',
					rows: [
						{ title: '', hours: 0, description: '' }
					]
				},
				{
					title: 'Backend-utveckling',
					rows: [
						{ title: '', hours: 0, description: '' }
					]
				},
				{
					title: 'Övrigt',
					rows: [
						{ title: 'Testning & Lansering', hours: 0, description: '' }
					]
				},

			]
		},

		{
			title: 'Grundinstallation',
			template: [
				{
					title: 'Grundinstallation',
					rows: [
						{
							title: "Installation Wordpress",
							hours: 1,
							description: 'Här sätter vi upp WordPress med vårat grundtema'
						},
						{
							title: "Lokal utvecklingsmiljö",
							hours: 1,
							description: 'Vi sätter upp vår lokala utvecklingsmiljö'
						},
						{
							title: "Versionshantering + Deploy",
							hours: 1,
							description: 'Här sätter vi upp versionshantering via BitBucket, och även automatisk deploy till vår utvecklingsmiljö.'
						},

					]
				}
			]
		},
		{
			title: 'Stor sajt',
			template:
			[
				{
					title: 'Övergripande',
					rows: [
						{
							title: "Projekt- & produktionsledning",
							hours: 20,
							description: ''
						},
						{
							title: "Förstudie, Strategi & SEO analys",
							hours: 10,
							description: ''
						}
					]
				},
				{
					title: 'Innehållsskisser & utbildning',
					rows: [
						{
							title: "Sitemap, wireframes & User Stories",
							hours: 16,
							description: ''
						},
						{
							title: "Utbildning WP-Admin",
							hours: 3,
							description: ''
						}
					]
				},
				{
					title: 'Design',
					rows: [
						{
							title: "Startsida, övergripande manér & koncept",
							hours: 24,
							description: ''
						},
						{
							title: "Landningssidor",
							hours: 16,
							description: ''
						},
						{
							title: "Undersidor",
							hours: 16,
							description: ''
						},
						{
							title: "Funktion- och interaktionsdesign",
							hours: 16,
							description: ''
						}
					]
				},
				{
					title: 'Utveckling frontend',
					rows: [
						{
							title: "Startsida",
							hours: 20,
							description: ''
						},
						{
							title: "Landningssidor",
							hours: 16,
							description: ''
						},
						{
							title: "Undersidor",
							hours: 16,
							description: ''
						},
						{
							title: "Funktion- och interaktionsdesign",
							hours: 16,
							description: ''
						},
						{
							title: "Mobilanpassning läsplatta",
							hours: 8,
							description: ''
						},
						{
							title: "Mobilanpassning mobil",
							hours: 8,
							description: ''
						}
					]
				},
				{
					title: 'Utveckling backend',
					rows: [
						{
							title: "Grundinstallation WP + Plugins",
							hours: 5,
							description: ''
						}
					]
				},
				{
					title: 'SEO + Övrigt',
					rows: [
						{
							title: "On-page SEO och meta-information",
							hours: 8,
							description: ''
						},
						{
							title: "Lansering/Testning",
							hours: 16,
							description: ''
						},
						{
							title: "Kvalitetssäkring webbläsare och enheter",
							hours: 16,
							description: ''
						},
						{
							title: "Funktionalitetstester, formulär, tracking-events mm.",
							hours: 8,
							description: ''
						},
						{
							title: "Ompekningar och domänhantering",
							hours: 1,
							description: ''
						},
						{
							title: "Serveradministration",
							hours: 1,
							description: ''
						}
					]
				},
			]
		}
	]