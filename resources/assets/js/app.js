// Dependencies
require("font-awesome-webpack")

import anime from 'animejs'
//import VeeValidate from 'vee-validate'
import Estimate from './components/Estimate.vue'
import EstimateNav from './components/EstimateNav.vue'
import EstimateList from './components/EstimateList.vue'
import EstimateControls from './components/EstimateControls.vue'

// Bindings
window.anime = anime
window.Vue = require('vue')
window._ = require('lodash')
window.Sortable = require('sortablejs')

Vue.component('Estimatenav', EstimateNav)
Vue.component('Estimate', Estimate)
Vue.component('Estimatelist', EstimateList)
Vue.component('Estimatecontrols', EstimateControls)

//Vue.use(VeeValidate)

/*Vue.directive('entertest', {
	
	inserted (el, binding) {
        $(el).selectpicker()
    },

    update (el, binding) {
        $(el).selectpicker('refresh').trigger("change");
    },

    unbind (el, binding) {
        $(el).off().selectpicker('destroy');
    }
})*/

window.Events = new Vue({})
