<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="UTF-8">
    <title>Estimatmaskinen</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>

    <!-- Adding a pointless comment to see what permissions are set when deploying -->

    <div id="app">

        <header>
            <div class="wrap">
                <svg style="fill: rgb(16, 16, 17);" id="dazy-logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" xml:space="preserve"><g><polygon fill="none" points="238,540 238,594 370.5,594 238,818 471.9,818 471.9,764 341.8,764 471.9,540     "></polygon><polygon fill="none" points="651.1,282.3 711.2,460 779.2,460 685,182 617.1,182 523,460 590.9,460    "></polygon><polygon fill="none" points="569.9,818 641.3,818 779.2,540 708.1,540 655.1,646.5 597.7,540 523,540 621.8,713.6  "></polygon><path fill="none" d="M484.6,321c0-75.1-52.4-139-141.8-139H238v278h104.8C432.6,460,484.6,396.1,484.6,321z M302.3,406V236h34.6
                c54.8,0,81.4,40.9,81.4,85c0,44.1-26.6,85-81.4,85H302.3z"></path><polygon fill="none" points="0,0 1000,0 1000,0 0,0  "></polygon><path d="M418.3,321c0-44.1-26.6-85-81.4-85h-34.6v170h34.6C391.7,406,418.3,365.1,418.3,321z"></path><path d="M0,1000h1000V0H0V1000z M471.9,764v54H238l132.5-224H238v-54h233.9L341.8,764H471.9z M617.1,182H685l94.1,278h-67.9
                l-60.2-177.7L590.9,460H523L617.1,182z M597.7,540l57.5,106.5L708.1,540h71.1L641.3,818h-71.5l51.9-104.4L523,540H597.7z M238,182
                h104.8c89.4,0,141.8,63.9,141.8,139s-52,139-141.8,139H238V182z"></path></g></svg>
                <span class="small">Estimatmaskinen™</span>
            </div>
        </header>

        <div class="wrap wrap--main" id="estimate-container">

            <h2 class="h4 divider">Tidsestimat</h2>

            <Estimate :id="id" :previewMode="previewMode"></Estimate>

        </div>

    </div>

<script src="js/app.js"></script>
<script>
    const app = new Vue({
        
        el: "#app",

        data: {
            @if(Auth::check())
            previewMode: false,
            @else
            previewMode: true,
            @endif
            id: '{{$id}}'
        },

        methods: {

            // togglePreviewMode () {
            //     Events.$emit('previewMode', this.previewMode = !this.previewMode);
            // }

        }

    })
</script>

</body>
</html>