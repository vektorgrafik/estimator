@extends('layouts.app')

@section('content')

<style type="text/css">
  .badge-success {
    background-color: #2ecc71;
    color: white;
  }
  .badge-warning {
    background-color: #d35400;
    color: white;
  }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default">
                <div class="panel-heading">Nytt citat</div>

                <div class="panel-body">
                    <form action="{{ url('/slack/new') }}" method="post">
                        {{ csrf_field() }}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @if (session('dupes'))
                            <div class="alert alert-warning">
                                Möjligtvis en dublett!
                            </div>
                            <h4>Möjliga dubletter</h4>
                            <table class="table">
                              @foreach(session('dupes') as $dupe)
                              <tr>
                                <td><p>{{ $dupe['user']->name }} har lagt till citatet: </span> <span class="text-muted">&quot;{{ $dupe['quote']->quote }}&quot;</span></p></td>
                              </tr>
                              @endforeach
                            </table>

                          <div class="form-group clearfix">
                              <input type="hidden" name="force_duplicate" value="1">
                              <input type="submit" class="btn btn-large btn-primary" value="Ingen dublett, spara ändå!">
                              <a class="btn btn-large btn-danger" href="/slack/new">Aj fan, nä då vill jag inte spara!</a>
                          </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12 col-sm-6">
                                <div class="form-group">
                                    <label>Citat</label>
                                    <textarea name="quote" id="" cols="30" rows="3" class="form-control" placeholder="Citat">{!! old('quote') !!}</textarea>
                                </div>                                
                            </div>
                            <div class="col-sm-12 col-sm-6">
                                <div class="form-group">
                                    <label>Kontext</label>
                                    <textarea name="quote_context" id="" cols="30" rows="3" class="form-control" placeholder="Kontext">{!! old('quote_context') !!}</textarea>
                                </div>                             
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
                                    <label for="">Rätt svar</label>
                                    <input list="users" autocomplete="off" type="text" class="form-control" name="answer_correct" value="{!! old('answer_correct') !!}">
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <label for="">Fel svar 1</label>
                                    <input list="users" autocomplete="off" type="text" class="form-control" name="answer_wrong_1" value="{!! old('answer_wrong_1') !!}">
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <label for="">Fel svar 2</label>
                                    <input list="users" autocomplete="off" type="text" class="form-control" name="answer_wrong_2" value="{!! old('answer_wrong_2') !!}">
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <label for="">Fel svar 3</label>
                                    <input list="users" autocomplete="off" type="text" class="form-control" name="answer_wrong_3" value="{!! old('answer_wrong_3') !!}">
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <label for="">Fel svar 4</label>
                                    <input list="users" autocomplete="off" type="text" class="form-control" name="answer_wrong_4" value="{!! old('answer_wrong_4') !!}">
                                </div>
                            </div>
                        </div>

                        <datalist id="users">
                          <option value="Andreas">
                          <option value="Calle">
                          <option value="Daniel">
                          <option value="Fredrik">
                          <option value="Frida">
                          <option value="Gergö">
                          <option value="Helene">
                          <option value="Håkan">
                          <option value="Håkan Öhman">
                          <option value="Jimmy">
                          <option value="Johanna">
                          <option value="Kent">
                          <option value="Magnus">
                          <option value="Mergim">
                          <option value="Kramnäs">
                          <option value="Olle">
                          <option value="Susanna">
                        </datalist>

                        <div class="form-group clearfix">
                            <input type="submit" class="btn btn-large btn-primary" value="Spara">
                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Dina citat</div>

                <div class="panel-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Citat</th>
                        <th style="text-align: right;">Publiceringsdatum</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if (count($quotes) > 0)
                        @foreach ($quotes as $quote )
                            <tr>
                              <td>&quot;{{ $quote->quote }}&quot;</td>
                              <td style="text-align: right;">
                                @if($quote->upcoming)
                                  <span class="badge badge-warning badge-pill">{{ $quote->date_publish }}</span>
                                @else
                                  <span class="badge badge-success badge-pill">{{ $quote->date_publish }}</span>
                                @endif
                                
                              </td>
                            </tr>
                        @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
