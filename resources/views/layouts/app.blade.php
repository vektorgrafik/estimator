<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>VG | Estimator</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="/css/app.css">

    <style>
        body {
            font-family: 'Lato';
            background-image: url(images/login.jpg);
            background-size: cover;
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="login-screen">
    <div class="overlay"></div>
    <div class="overlay-two"></div>
    @yield('content')

</body>
</html>
